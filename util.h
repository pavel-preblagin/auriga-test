#ifndef __UTIL_H__
#define __UTIL_H__

#include <sys/types.h>
#include <fts.h>

FTS *fts_open_wrap(char *argv[], int options, int (*cmp)(const FTSENT **, const FTSENT **),
							int empty);

char *get_username(uid_t uid);
char *get_group(gid_t gid);
void mode2str(mode_t mode, char *buf);

#endif