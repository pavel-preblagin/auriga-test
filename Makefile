APP     := lsl
OBJS    := $(addsuffix .o, lsl util)
HDR     := $(addsuffix .h, util)
CFLAGS  := -Wall
LDFLAGS :=
LIBS    :=

.PHONY: clean

all: lsl

lsl: $(OBJS)
	$(CC) $(LDFLAGS) -o $(APP) $(OBJS) $(LIBS)
	chmod +x $(APP)

%.o : %.c $(HDR)
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -f *.o $(APP)
