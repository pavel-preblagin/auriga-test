#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fts.h>
#include <pwd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "util.h"

/* sort by name */
int cmp_name(const FTSENT** a, const FTSENT** b)
{
    return (strcmp((*a)->fts_name, (*b)->fts_name));
}

static void print(FTSENT *parent, FTSENT *children, int *retval)
{
	FTSENT *ptr;
	unsigned int cnt = 0;

	if(children == NULL)
		return;

	if(parent && parent->fts_info == FTS_D) { /* print a name of the listed directory */
		printf("%s:\n", parent->fts_path);
	}

	for(ptr = children; ptr; ptr = ptr->fts_link) {
		struct stat *stat = ptr->fts_statp;
		char *user, *group, lastmod[20], mode[12] = {0}; /* defines for len? */

		if (ptr->fts_info == FTS_ERR || ptr->fts_info == FTS_NS) { /* something has gone wrong */
			fprintf(stderr, "cannot access '%s': %s\n", ptr->fts_name, strerror(ptr->fts_errno));
			*retval = ptr->fts_errno;
			continue;
		}

		if(parent == NULL && ptr->fts_info == FTS_D) { /* skip directory which will be listed */
			continue;
		}

		if(*ptr->fts_name == '.') { /* skip hidden files */
			continue;
		}

		user  = get_username(stat->st_uid);
		group = get_group(stat->st_gid);
		mode2str(stat->st_mode, mode);
		strftime(lastmod, sizeof(lastmod), "%Y-%m-%d %H:%M", localtime(&stat->st_mtime));

		/* TODO: pretty formating with calculated column's len, separate multiple output */
		printf("%s %lu %s %s %lu %s %s\n", mode, stat->st_nlink, user, group,
				stat->st_size, lastmod, ptr->fts_name);
		cnt++;
	}

	if(parent) /* directory items count */
		printf("count: %u\n", cnt);
}

static void lsl(int argc, char *argv[], int opt, int *retval)
{
	FTS *stream;
	FTSENT *parent;

	stream = fts_open_wrap(argv, opt, cmp_name, argc <= 0);
	if(stream == NULL) {
		perror("fts_open");
		*retval = errno;
		return;
	}

	print(NULL, fts_children(stream, 0), retval); /* print files */

	while((parent = fts_read(stream))) { /* print children */
		FTSENT *children;
		switch(parent->fts_info) {
			case FTS_D:
				children = fts_children(stream, 0);
				print(parent, children, retval);
				if(children)
					fts_set(stream, parent, FTS_SKIP); /* avoid recursive */
				break;
			case FTS_DC:
				fprintf(stderr, "'%s': directory causes a cycle in the tree\n", parent->fts_name); /* it's a error? */
				break;
			case FTS_DNR:
			case FTS_ERR:
				fprintf(stderr, "cannot access '%s': %s\n", parent->fts_name, strerror(parent->fts_errno));
				*retval = parent->fts_errno;
				break;
		}
	}

	fts_close(stream);
}

int main(int argc, char *argv[])
{
	int opt = 0, ret = 0;
	lsl(--argc, ++argv, opt, &ret);
	return ret;
}
