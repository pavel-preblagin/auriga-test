#include <sys/types.h>
#include <sys/stat.h>
#include <fts.h>
#include <grp.h>
#include <pwd.h>
#include <string.h>

/* common wrapper for fts_open(), set the default value if needs */
FTS *fts_open_wrap(char *argv[], int options, int (*cmp)(const FTSENT **, const FTSENT **),
							int empty)
{
	if(empty) {
		char *default_argv[] = {".", NULL}; /* define this? */
		return fts_open(default_argv, options, cmp);
	}
	return fts_open(argv, options, cmp);
}

char *get_username(uid_t uid)
{
	struct passwd *pwd = getpwuid(uid);
	if(pwd) {
		return pwd->pw_name;
	}
	return "???"; /* TODO: return string with uid ? */
}

char *get_group(gid_t gid)
{
	struct group *grp = getgrgid(gid);
	if(grp) {
		return grp->gr_name;
	}
	return "???"; /* TODO: return string with gid ? */
}

static inline char get_filetype(mode_t mode)
{
	switch (mode & S_IFMT) {
		case S_IFBLK:
			return 'b';
		case S_IFCHR:
			return 'c';
		case S_IFDIR:
			return 'd';
		case S_IFIFO:
			return 'p';
		case S_IFLNK:
			return 'l';
		case S_IFREG:
			return '-';
		case S_IFSOCK:
			return 's';
	}
	return '?';
}

static void get_rights(mode_t mode, char *buf)
{
	/*
		NOTE: it's a pretty and short code but it comes from the assumption 
		that mode's offsets are the same everythere.
	*/
	const char *sets[] = {"---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx"};
	strcpy(buf, sets[(mode >> 6) & 7]);
	strcpy(buf + 3, sets[(mode >> 3) & 7]);
	strcpy(buf + 6, sets[(mode & 7)]);
}

void mode2str(mode_t mode, char *buf)
{
	buf[0] = get_filetype(mode);
	get_rights(mode, ++buf);
}